<?php

namespace App\Imports;

use App\Models\PropertyAnalytic;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PropertyAnalyticsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PropertyAnalytic([
            'property_id' => $row['property_id'],
            'analytic_type_id' => $row['analytic_type_id'],
            'value' => $row['value'],
        ]);
    }
}
