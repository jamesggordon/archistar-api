<?php

namespace App\Imports;

use App\Models\AnalyticType;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AnalyticTypesImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AnalyticType([
            'id' => $row['id'],
            'name' => $row['name'],
            'units' => $row['units'],
            'is_numeric' => $row['is_numeric'],
            'num_decimal_places' => $row['num_decimal_places'],
        ]);
    }
}
