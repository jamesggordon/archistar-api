<?php

namespace App\Imports;

use App\Models\Property;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PropertiesImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Property([
            'guid' => uniqid(),
            'id' => $row['property_id'],
            'suburb' => $row['suburb'],
            'state' => $row['state'],
            'country' => $row['country'],
        ]);
    }
}
