<?php

namespace App\Reports;

use App\Models\AnalyticType;
use App\Models\Property;
use Exception;
use Illuminate\Support\Facades\DB;

class Summary implements SummaryContract
{
    /**
     * Calculate the requested Summary data.
     * 
     * Please note that this solution does NOT simply retrieve the list of values from the database and then
     * process that list internally, as that will not scale. Instead, all results are calculated by the
     * database and the results passed back. Of course, depending on what additional summary information
     * is required in the future, this approach may not be possible and we may need to consider alternatives.
     * This solution also relies on some MySQL trickery, such as casting text fields to float fields on the
     * fly and there is no guarantee this facility would be available on all databases.

     * Also note that I have chosen to implement the database access using raw SQL rather than Eloquent.
     * Although it would have been possible in Eloquent, I feel it would have been much more verbose
     * and, by virtue of that, less readable. The other benefit of going with raw SQL is that debugging
     * is easier because you don't have to hunt around in the logs to find the SQL that Eloquent created.
     * Instead, you can simply copy the SQL templates out of this source, paste them into your database
     * editor of choice, fill in the values and then you can quickly check the queries, query plans, etc.
     * 
     * @param  string $attribute
     * @param  string $criteria
     * @param  int    $analytic
     * @return array
     */
    public function calculate(string $attribute, string $criteria, int $analytic)
    {
        if (! in_array($attribute, Property::getAttributesAvailableForSummaryReport())) {
            return $this->emptyResponse();
        }

        $analyticType = AnalyticType::find($analytic);

        if ($analyticType == null) {
            return $this->emptyResponse();
        }

        if (! $analyticType->is_numeric) {
            throw new Exception('Cannot request summary data for non-numeric analytics');
        }

        // Count all properties that satisfy the search criteria - expecting exactly one row
        $result = DB::select ("SELECT count(*) cnt
                               FROM   properties
                               WHERE  {$attribute} = ?",
                               [$criteria]);

        if (count($result) != 1) {
            throw new Exception('Database error 001');
        }

        $num_properties = $result[0]->cnt;

        if ($num_properties == 0) {
            return $this->emptyResponse();
        }
 
        // Retrieve analytic information for properties that satisfy the search criteria - expecting exactly one or zero rows
        $result = DB::select ("SELECT min(cast(value as float)) min_value, max(cast(value as float)) max_value, count(*) num_values
                               FROM   property_analytics
                               JOIN   properties ON (properties.id = property_id)
                               WHERE  {$attribute} = ?
                               AND    analytic_type_id = ?",
                               [$criteria, $analytic]);

        if (count($result) > 1) {
            throw new Exception('Database error 002');
        }

        if (count($result) == 0) {
            return $this->emptyResponse();
        }

        $min_value = number_format($result[0]->min_value, $analyticType->num_decimal_places, '.', '');
        $max_value = number_format($result[0]->max_value, $analyticType->num_decimal_places, '.', '');

        $with_values = $result[0]->num_values;
        $percentage_with = round($with_values * 100 / $num_properties);
        $percentage_without = 100 - $percentage_with;

        $mid1 = floor(($with_values + 1) / 2);
        $mid2 = ceil(($with_values + 1) / 2);

        $even_number_of_rows = $mid1 != $mid2;

        // Find the median by getting the database to retrieve the middle value(s)
        if ($even_number_of_rows) {
            $result = DB::select ("SELECT value
                                   FROM   property_analytics
                                   JOIN   properties ON (properties.id = property_id)
                                   WHERE  {$attribute} = ?
                                   AND    analytic_type_id = ?
                                   ORDER BY cast(value as float)
                                   LIMIT  2
                                   OFFSET ?",
                                   [$criteria, $analytic, $mid1 - 1]);

            if (count($result) != 2) {
                throw new Exception('Database error 003');
            }

            $median_value = round(($result[0]->value + $result[1]->value) / 2, $analyticType->num_decimal_places);
            $median_value = number_format($median_value, $analyticType->num_decimal_places, '.', '');
        }
        else {
            $result = DB::select ("SELECT value
                                   FROM   property_analytics
                                   JOIN   properties ON (properties.id = property_id)
                                   WHERE  {$attribute} = ?
                                   AND    analytic_type_id = ?
                                   ORDER BY cast(value as float)
                                   LIMIT  1
                                   OFFSET ?",
                                   [$criteria, $analytic, $mid1 - 1]);

            if (count($result) != 1) {
                throw new Exception('Database error 004');
            }

            $median_value = number_format($result[0]->value, $analyticType->num_decimal_places, '.', '');
        }

        return [
            'name'          => (string) $analyticType->name,
            'minimum_value' => (string) $min_value,
            'maximum_value' => (string) $max_value,
            'median_value'  => (string) $median_value,
            'percentage_with_value' => (string) $percentage_with,
            'percentage_without_value' => (string) $percentage_without,
        ];
    }

    /**
     * Return an empty response to the caller.
     * 
     * @return array
     */
    private function emptyResponse()
    {
        return [
            'message'       => 'No data available',
            'minimum_value' => 0,
            'maximum_value' => 0,
            'median_value'  => 0,
            'percentage_with_value' => 0,
            'percentage_without_value' => 100,
        ];
    }
}
