<?php

namespace App\Reports;

use App\Models\AnalyticType;
use App\Models\Property;
use App\Models\PropertyAnalytic;
use Exception;

class SummaryV2 implements SummaryContract
{
    /**
     * Calculate the requested Summary data.
     * 
     * @param  string $attribute
     * @param  string $criteria
     * @param  int    $analytic
     * @return array
     */
    public function calculate(string $attribute, string $criteria, int $analytic)
    {
        if (! in_array($attribute, Property::getAttributesAvailableForSummaryReport())) {
            return $this->emptyResponse();
        }

        $analyticType = AnalyticType::find($analytic);

        if ($analyticType == null) {
            return $this->emptyResponse();
        }

        if (! $analyticType->is_numeric) {
            throw new Exception('Cannot request summary data for non-numeric analytics');
        }

        $num_properties = Property::where($attribute, $criteria)->count();

        if ($num_properties == 0) {
            return $this->emptyResponse();
        }

        $values = PropertyAnalytic::
            where('analytic_type_id', $analytic)->
            join('properties', 'properties.id', 'property_id')->
            where($attribute, $criteria)->
            pluck('value');

        $values = $values->map(function($value) {
            return (float) $value;
        });

        $values = $values->sort();

        $min_value = number_format($values->min(), $analyticType->num_decimal_places, '.', '');
        $max_value = number_format($values->max(), $analyticType->num_decimal_places, '.', '');
        $median_value = number_format($values->median(), $analyticType->num_decimal_places, '.', '');

        $with_values = $values->count();
        $percentage_with = round($with_values * 100 / $num_properties);
        $percentage_without = 100 - $percentage_with;

        return [
            'name'          => (string) $analyticType->name,
            'minimum_value' => (string) $min_value,
            'maximum_value' => (string) $max_value,
            'median_value'  => (string) $median_value,
            'percentage_with_value' => (string) $percentage_with,
            'percentage_without_value' => (string) $percentage_without,
        ];
    }

    /**
     * Return an empty response to the caller.
     * 
     * @return array
     */
    private function emptyResponse()
    {
        return [
            'message'       => 'No data available',
            'minimum_value' => 0,
            'maximum_value' => 0,
            'median_value'  => 0,
            'percentage_with_value' => 0,
            'percentage_without_value' => 100,
        ];
    }
}
