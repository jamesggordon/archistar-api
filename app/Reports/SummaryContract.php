<?php

namespace App\Reports;

interface SummaryContract
{
    /**
     * Calculate the requested Summary data.
     * 
     * @param  string $attribute
     * @param  string $criteria
     * @param  int    $analytic
     * @return array
     */
    public function calculate(string $attribute, string $criteria, int $analytic);
}
