<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Property extends Model
{
    use HasFactory;

    protected $fillable = ['suburb', 'state', 'country'];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($property) {
          $property->guid = Uuid::uuid4();
        });

        self::deleting(function($property) {
            PropertyAnalytic::where('property_id', $property->id)->delete();
        });
    }

    static public function getAttributesAvailableForSummaryReport()
    {
        return ['suburb', 'state', 'country'];
    }

    public function analytics()
    {
        return $this->hasMany('App\Models\PropertyAnalytic');
    }
}
