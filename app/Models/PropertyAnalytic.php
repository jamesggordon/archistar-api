<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyAnalytic extends Model
{
    use HasFactory;

    protected $fillable = ['value'];

    public function analyticType()
    {
        return $this->belongsTo('\App\Models\AnalyticType');
    }

    /**
     * Enforce the decimal place specification set in the AnalyticType.
     *
     * @param  string  $value
     * @return void
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = number_format($value, $this->analyticType->num_decimal_places, '.', '');
    }
}
