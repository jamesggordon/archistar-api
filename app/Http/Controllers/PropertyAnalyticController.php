<?php

namespace App\Http\Controllers;

use App\Http\Resources\PropertyAnalytic as PropertyAnalyticResource;
use App\Models\PropertyAnalytic;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyAnalyticController extends Controller
{
    /**
     * Store a newly created PropertyAnalytic in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $analytic = new PropertyAnalytic;
        $analytic->property_id = $request->property;
        $analytic->analytic_type_id = $request->analytic_type_id;
        $analytic->value = $request->value;
        $analytic->save();
   
        return response()->json([
            'created' => true,
            'data' => $analytic,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified PropertyAnalytic.
     * 
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyAnalytic $analytic)
    {
        return new PropertyAnalyticResource($analytic);
    }

    /**
     * Update the specified PropertyAnalytic.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PropertyAnalytic  $analytic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyAnalytic $analytic)
    {
        if ($analytic->update($request->all())) {
            return response()->json([
                'updated' => true,
                'data' => $analytic,
            ], Response::HTTP_OK);
        }

        return response()->json([
            'updated' => false,
        ], Response::HTTP_OK);
    }

    /**
     * Expunge the specified PropertyAnalytic.
     *
     * @param  \App\Models\PropertyAnalytic  $analytic
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyAnalytic $analytic)
    {
        $deleted = (boolean) $analytic->delete();

        return response()->json([
            'deleted' => $deleted,
        ], Response::HTTP_OK);
    }
}
