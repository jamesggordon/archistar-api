<?php

namespace App\Http\Controllers;

use App\Http\Resources\Property as PropertyResource;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyController extends Controller
{
    /**
     * Display a listing of all Properties.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('limit') || $request->has('page')) {
            return ['data' => PropertyResource::collection(Property::simplePaginate($request->input('limit')))->keyBy('id')];
        } else {
            return PropertyResource::collection(Property::all()->keyBy->id);
        }
    }

    /**
     * Store a newly created Property in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property;
        $property->suburb  = $request->suburb;
        $property->state   = $request->state;
        $property->country = $request->country;
        $property->save();
   
        return response()->json([
            'created' => true,
            'data' => $property,
        ], Response::HTTP_CREATED);
    }

    /**
     * Retrieve the specified Property.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        return new PropertyResource($property);
    }

    /**
     * Update the specified Property.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        if ($property->update($request->all())) {
            return response()->json([
                'updated' => true,
                'data' => $property,
            ], Response::HTTP_OK);
        }

        return response()->json([
            'updated' => false,
        ], Response::HTTP_OK);
    }

    /**
     * Expunge the specified Property.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $deleted = (boolean) $property->delete();

        return response()->json([
            'deleted' => $deleted,
        ], Response::HTTP_OK);
    }
}
