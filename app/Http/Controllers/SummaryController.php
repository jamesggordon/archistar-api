<?php

namespace App\Http\Controllers;

use App\Reports\SummaryContract;
use Illuminate\Http\Response;

class SummaryController extends Controller
{
    /**
     * Display the requested Summary data.
     * 
     * @param  string $dimension
     * @param  string $criteria
     * @param  int    $analytic
     * @return \Illuminate\Http\Response
     */
    public function show(string $dimension, string $criteria, int $analytic, SummaryContract $summary)
    {
        return response()->json([
            'summary' => $summary->calculate($dimension, $criteria, $analytic),
        ], Response::HTTP_OK);
    }
}
