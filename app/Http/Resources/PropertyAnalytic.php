<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropertyAnalytic extends JsonResource
{
    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;
    
    /**
     * Transform the PropertyAnalytic into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at->toISOString(),
            'updated_at' => $this->updated_at->toISOString(),
            'property_id' => $this->property_id,
            'value' => $this->value,
            'name' => $this->analyticType->name,
            'units' => $this->analyticType->units,
            'is_numeric' => $this->analyticType->is_numeric,
            'num_decimal_places' => $this->analyticType->num_decimal_places,
        ];
    }
}
