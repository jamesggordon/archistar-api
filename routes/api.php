<?php

use App\Http\Controllers\PropertyAnalyticController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\SummaryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if (App::environment() == 'local') {
    Route::apiResource('property', PropertyController::class);
    Route::apiResource('property.analytic', PropertyAnalyticController::class)->except(['index', 'create'])->shallow();
    Route::get('summary/{attribute}/{critera}/{analytic}', [SummaryController::class, 'show']);
}
else {
    Route::middleware('auth:sanctum')->apiResource('property', PropertyController::class);
    Route::middleware('auth:sanctum')->apiResource('property.analytic', PropertyAnalyticController::class)->except(['index', 'create'])->shallow();
    Route::middleware('auth:sanctum')->get('summary/{attribute}/{critera}/{analytic}', [SummaryController::class, 'show']);
}
