<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Archistar API</title>

        <style>
            body {
                font-family: 'Arial';
            }
        </style>
    </head>
    <body>
      <p><a href="https://bitbucket.org/idda/coding-challenges/src/master/BackEnd-Engineer.md">Click here for futher details</a></p>
    </body>  
</html>
