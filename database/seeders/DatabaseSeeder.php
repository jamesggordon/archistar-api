<?php

namespace Database\Seeders;

use App\Imports\AnalyticTypesImport;
use App\Imports\PropertiesImport;
use App\Imports\PropertyAnalyticsImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new AnalyticTypesImport, base_path('database/testdata/BackEndTest_TestData_v1.1-AnalyticTypes.xlsx'));
        Excel::import(new PropertiesImport, base_path('database/testdata/BackEndTest_TestData_v1.1-Properties.xlsx'));
        Excel::import(new PropertyAnalyticsImport, base_path('database/testdata/BackEndTest_TestData_v1.1-PropertyAnalytics.xlsx'));
    }
}
