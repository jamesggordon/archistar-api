<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalyticTypesTable extends Migration
{
    public function up()
    {
        Schema::create('analytic_types', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->enum('units', ['m', 'm2', ':1']);
            $table->boolean('is_numeric');
            $table->unsignedTinyInteger('num_decimal_places');
        });
    }

    public function down()
    {
        Schema::dropIfExists('analytic_types');
    }
}
