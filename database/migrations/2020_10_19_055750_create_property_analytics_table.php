<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyAnalyticsTable extends Migration
{
    public function up()
    {
        Schema::create('property_analytics', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('property_id')->constrained();
            $table->foreignId('analytic_type_id')->constrained();
            $table->text('value'); // TODO: Check maximum storage requirements for analytic values, because 64K seems excessive
            $table->unique(['property_id', 'analytic_type_id'], 'property_analytic_type_index');
        });
    }

    public function down()
    {
        Schema::dropIfExists('property_analytics');
    }
}
