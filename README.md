
![Archistar](https://archistar.ai/wp-content/uploads/2019/08/archi.svg)

# Archistar Back-end Coding Challenge Solution

Details of this challenge can be found [here](https://bitbucket.org/idda/coding-challenges/src/master/BackEnd-Engineer.md).

## Requirements

This application has a minimum requirement of PHP 7.3, Composer and any currently supported version of MySQL or MariaDB.

## Installation

Use the following commands to install and configure the application and run the tests:

```
git clone git@bitbucket.org:jamesggordon/archistar-api.git

cd archistar-api

composer install

cp .env.example .env

php artisan key:generate

# Configure your database connection in the .env file

php artisan test
```

## API

Use ``php artisan route:list`` to view supported routes, which are summarised below:

| Method | URI                                          | Description                                      |
|--------|----------------------------------------------|--------------------------------------------------|
| GET    | api/property                                 | Retrieve the full list of properties             |
| POST   | api/property                                 | Store a new property                             |
| GET    | api/property/{property}                      | Retrieve an existing property                    |
| PATCH  | api/property/{property}                      | Update the details of an existing property       |
| DELETE | api/property/{property}                      | Expunge (hard delete) an existing property       |
| POST   | api/property/{property}/analytic             | Add analytic information to an existing property |
| GET    | api/analytic/{analytic}                      | Retrieve full details of a specific analytic     |
| PATCH  | api/analytic/{analytic}                      | Update the value of an existing analytic         |
| DELETE | api/analytic/{analytic}                      | Expunge (hard delete) an existing analytic       |
| GET    | api/summary/{attribute}/{critera}/{analytic} | Retrieve aggregated snapshots of analytic data   |

>*NOTE:* Although it is possible to update an analytic for a property, the only attribute that can be changed is the ``value`` attribute. The rationale for this decision is that the update endpoint will most likely be used to correct the _value_ of an analytic, not change the _type_ of the analytic. For example, if a floorspace ratio was added but what was actually needed was the maximum building height, I would imagine the user would be deleting the floorspace ratio analytic and then adding a maximum building height analytic. I cannot imagine the user interface being designed in such a way that the user could change the _type_ of an analytic in a single operation.

## Security

A very minimal configuration of Laravel Sanctum has been incorporated to add simple, token-based authentication for the API endpoints. It is very simplistic and nowhere near production quality; this work was done mainly to explore how to write automated tests incorporating Sanctum. Please note that the routes are registered _without_ the authentication middleware in the ``local`` environment. This makes life easier during early development as it means the endpoints can be hit without worrying about having to constantly generate API tokens and include them in the request headers. Just don't forget to set the environment to production when you go live!

## Local Development

In order to test the API locally, simply run ``php artisan migrate:fresh --seed`` and then use ``curl`` to issue requests, e.g.:

```
php artisan migrate:fresh --seed

curl http://projectname.test/api/property/1

curl http://projectname.test/api/summary/country/Australia/1
```

## Linting and Testing

The [TightenCo Linter](https://github.com/tighten/tlint) has been utilised to ensure the code adheres to a standard that is reasonable for a Laravel project. The linter can be run using the following command:

```
./vendor/bin/tlint
```

PHPCodeSniffer should also be added, if for no other reason than to ensure that PSR-2 coding standards are being followed.

There are currently three levels of testing included in the project:

1. ``tests/Unit`` currently contains just one test but is intended to house all isolated tests.
2. ``tests/Functional`` is intended for API endpoint tests and there should be at least one happy path test and several failure scenario tests for each endpoint.
3. ``tests/Feature`` currently contains one end-to-end test.

Tests can be run locally using ``php artisan test`` and are also run by the Bitbucket pipeline.

## Logging

Although the scoring criteria for this challenge mentions logging, nothing specific has been implemented. Some options to consider would be:

1. Adding a middleware that can log all incoming HTTP requests.
2. Adding additional methods to the ``boot()`` methods of each Model class to log certain events.
3. Enabling database query logging, at least for development, as this can be very helpful.
4. Utilising the options Laravel provides for implementing custom exception classes.
5. Adjusting the log configuration to notify developers of critical errors via Slack or other channels.

## Known Issues

In my original solution to the analytics summary part, I tried to be smart and avoid selecting the entire list of analytic values from the database. I achieved this by relying on the ``cast`` keyword in SQL and managed to calculate the median value using SQL queries. However, I don't think this keyword was supported by MySQL 5.7, which is what the pipeline was using. I changed to a slightly different SQL trick (using ``abs()``) and the pipeline still failed. However, it turns out the ``abs()`` trick _did_ work, but there was a genuine bug elsewhere in the code that wasn't manifesting itself when the tests were run locally. However, before I figured that out, I wrote a second version of the solution, this time utilising Eloquent and the awesome helper methods available on the ``Collection`` object. Oh man! Why didn't I do this in the first place?!? Rather than hardwire the new class in the controller method, I decided to be slightly tricky and bind the ``SummaryV2`` class to the ``SummaryContract`` interface in the ``AppServiceProvider`` class to demonstrate a modicum of knowledge about the Laravel Service Container, as it really is a thing of beauty and deserves a shout-out. However, I don't think ``AppServiceProvider`` is the right place to do this binding, which is an issue. I also duplicated the ``emptyResponse()`` function, violating the DRY principle.

The project does not currently include any way to create an API key. This would be the next thing to work on after addressing the pipeline issue.

Although there is some validation of incoming data, additional validation should be included (along with boundary tests for the validations).
