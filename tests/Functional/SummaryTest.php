<?php

namespace Tests\Functional;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SummaryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the INDEX route.
     * 
     * A basic functional test. Laravel boilerplate only includes directories for Unit and Feature tests
     * by default, but adding a third directory for functional tests is very straightforward and is a nice
     * way to separate simple API endpoint tests from the more complicated e2e tests in the Feature directory.
     *
     * @return void
     */
    function testTheBigKahuna()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        // Check that we can't get summary data for attributes that aren't stored in the database
        $response = $this->get('api/summary/postcode/2040/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $this->assertEquals('No data available', $data['summary']['message']);

        // Check that we can't get summary data for suburbs for which we have no data
        $response = $this->get('api/summary/suburb/Lithgow/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $this->assertEquals('No data available', $data['summary']['message']);

        // Check that we can't get summary data for analytics that aren't stored in the database
        $response = $this->get('api/summary/suburb/Parramatta/4');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $this->assertEquals('No data available', $data['summary']['message']);

        // Now we start checking happy paths...
        $response = $this->get('api/summary/suburb/Parramatta/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('max_Bld_Height_m', '10.0', '29.0', '19.0', '100', '0');
        $this->assertSame($reference, $data);

        // Another happy path test...
        $response = $this->get('api/summary/suburb/Parramatta/2');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('min_lot_size_m2', '184', '1101', '823', '64', '36');
        $this->assertSame($reference, $data);

        // And one more happy path test...
        $response = $this->get('api/summary/suburb/Parramatta/3');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('fsr', '1.07', '3.35', '2.86', '50', '50');
        $this->assertSame($reference, $data);

        // Boundary test: only one row for the requested suburb and analytic
        $response = $this->get('api/summary/suburb/Castle Hill/3');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('fsr', '0.86', '0.86', '0.86', '100', '0');
        $this->assertSame($reference, $data);

        // Test that we are correctly sorting text fields as numbers
        $response = $this->get('api/summary/suburb/Subiaco/3');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('fsr', '1.34', '2.58', '2.48', '50', '50');
        $this->assertSame($reference, $data);

        // Test state summaries
        $response = $this->get('api/summary/state/NSW/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('max_Bld_Height_m', '10.0', '38.0', '21.0', '100', '0');
        $this->assertSame($reference, $data);

        // Test country summaries
        $response = $this->get('api/summary/country/Australia/2');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('min_lot_size_m2', '184', '1103', '626', '67', '33');
        $this->assertSame($reference, $data);
    }

    function package($name, $min, $max, $median, $with, $without)
    {
        $reference['summary']['name'] = $name;
        $reference['summary']['minimum_value'] = $min;
        $reference['summary']['maximum_value'] = $max;
        $reference['summary']['median_value'] = $median;
        $reference['summary']['percentage_with_value'] = $with;
        $reference['summary']['percentage_without_value'] = $without;

        return $reference;
    }
}
