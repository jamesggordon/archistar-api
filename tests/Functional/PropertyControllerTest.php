<?php

namespace Tests\Functional;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PropertyControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the INDEX route.
     * 
     * A basic functional test. Laravel boilerplate only includes directories for Unit and Feature tests
     * by default, but adding a third directory for functional tests is very straightforward and is a nice
     * way to separate simple API endpoint tests from the more complicated e2e tests in the Feature directory.
     *
     * @return void
     */
    function testIndex()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        $response = $this->get('api/property');
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertNotEquals(null, $array);
        $this->assertEquals(100, count($array['data']));
        $this->assertEquals(2.51, $array['data'][100]['analytics'][2]['value']);
        $this->assertEquals(2, $array['data'][100]['analytics'][2]['num_decimal_places']);
    }

    /**
     * Test the pagination on the INDEX route.
     * 
     * Since we added the pagination feature, we should probably test it :-)
     *
     * @return void
     */
    function testPaginationOnIndex()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        $response = $this->get('api/property?limit=10&page=10');
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertNotEquals(null, $array);
        $this->assertEquals(10, count($array['data']));
        $this->assertEquals(2.51, $array['data'][100]['analytics'][2]['value']);
        $this->assertEquals(2, $array['data'][100]['analytics'][2]['num_decimal_places']);
    }

    /**
     * Test the STORE, SHOW, UPDATE and DELETE routes.
     * 
     * Create a Property and check that the response contains the data we sent in the request. While we're
     * there we might as well retrieve the Property we just created and check that the attributes came back OK.
     * 
     * @return void
     */
    function testCrud()
    {
        // Test authentication
        $response = $this->postJson('api/property', ['suburb' => 'Leichhardt', 'state' => 'NSW', 'country' => 'Australia']);
        $response->assertStatus(401);

        Sanctum::actingAs(User::factory()->create(), ['*']);

        // Test STORE method
        $response = $this->postJson('api/property', ['suburb' => 'Leichhardt', 'state' => 'NSW', 'country' => 'Australia']);

        $response->assertStatus(201);
        $response->assertJson(['created' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals('Leichhardt', $array['data']['suburb']);
        $this->assertEquals('NSW', $array['data']['state']);
        $this->assertEquals('Australia', $array['data']['country']);

        // Test SHOW method
        $id = $array['data']['id'];

        $response = $this->get('api/property/' . $id);
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals('Leichhardt', $array['data']['suburb']);
        $this->assertEquals('NSW', $array['data']['state']);
        $this->assertEquals('Australia', $array['data']['country']);

        // Test UPDATE method
        $response = $this->patchJson('api/property/' . $id, ['suburb' => 'Petersham']);

        $response->assertStatus(200);
        $response->assertJson(['updated' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals('Petersham', $array['data']['suburb']);
        $this->assertEquals('NSW', $array['data']['state']);
        $this->assertEquals('Australia', $array['data']['country']);

        $response = $this->get('api/property/' . $id);
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals('Petersham', $array['data']['suburb']);
        $this->assertEquals('NSW', $array['data']['state']);
        $this->assertEquals('Australia', $array['data']['country']);

        // Test DELETE method
        $response = $this->delete('api/property/' . $id);

        $response->assertStatus(200);
        $response->assertJson(['deleted' => true]);

        $response = $this->get('api/property/' . $id);
        $response->assertStatus(404);
    }
}
