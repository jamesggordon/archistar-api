<?php

namespace Tests\Functional;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PropertyAnalyticControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the STORE, SHOW and UPDATE and DELETE routes.
     * 
     * Create a Property and check that the response contains the data we sent in the request. While we're
     * there we might as well retrieve the Property we just created and check that the attributes came back OK.
     * 
     * @return void
     */
    function testCrud()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        // Test STORE method
        $response = $this->postJson('api/property/21/analytic', ['analytic_type_id' => '3', 'value' => 1/8]);

        $response->assertStatus(201);
        $response->assertJson(['created' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals(0.13, $array['data']['value']);
        $this->assertEquals('fsr', $array['data']['analytic_type']['name']);
        $this->assertEquals(':1', $array['data']['analytic_type']['units']);

        // Test SHOW method
        $id = $array['data']['id'];

        $response = $this->get('api/analytic/' . $id);
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals(21, $array['data']['property_id']);
        $this->assertEquals(0.13, $array['data']['value']);
        $this->assertEquals(1, $array['data']['is_numeric']);
        $this->assertEquals(2, $array['data']['num_decimal_places']);

        // Test UPDATE method
        $response = $this->patchJson('api/analytic/' . $id, ['value' => 1/3]);

        $response->assertStatus(200);
        $response->assertJson(['updated' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals(0.33, $array['data']['value']);

        $response = $this->get('api/analytic/' . $id);
        $response->assertStatus(200);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals(0.33, $array['data']['value']);

        // Test DELETE method
        $response = $this->delete('api/analytic/' . $id);

        $response->assertStatus(200);
        $response->assertJson(['deleted' => true]);

        $response = $this->get('api/analytic/' . $id);
        $response->assertStatus(404);
    }

    /**
     * Test the foreign constraints on the PropertyAnalytics table.
     * 
     * @return void
     */
    function testConstraints()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        // Only one analytic type per property
        $response = $this->postJson('api/property/1/analytic', ['analytic_type_id' => '1', 'value' => 10]);
        $response->assertStatus(500);

        // Analytic type must exist
        $response = $this->postJson('api/property/1/analytic', ['analytic_type_id' => '4', 'value' => 10]);
        $response->assertStatus(500);

        // Property must exist
        $response = $this->postJson('api/property/101/analytic', ['analytic_type_id' => '1', 'value' => 10]);
        $response->assertStatus(500);
    }
}
