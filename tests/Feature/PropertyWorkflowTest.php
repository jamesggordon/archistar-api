<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PropertyWorkflowTest extends TestCase
{
    use RefreshDatabase;

    /**
     * We'll do a more complicated end-to-end test here, where we will create some properties, add some analytics
     * and then check the maths behind the summary endpoints. We will use a combination of existing and new data.
     *
     * @return void
     */
    function testPropertyWorkflow()
    {
        $this->seed();
        Sanctum::actingAs(User::factory()->create(), ['*']);

        // Create a new property
        $response = $this->postJson('api/property', ['suburb' => 'Ryde', 'state' => 'NSW', 'country' => 'Australia']);

        $response->assertStatus(201);
        $response->assertJson(['created' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals('Ryde', $array['data']['suburb']);
        $this->assertEquals('NSW', $array['data']['state']);
        $this->assertEquals('Australia', $array['data']['country']);

        $property_id = $array['data']['id'];

        // Check the analytics for this suburb
        $response = $this->get('api/summary/suburb/Ryde/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('max_Bld_Height_m', '10.0', '34.0', '34.0', '83', '17');
        $this->assertSame($reference, $data);

        // Add some analytics
        $response = $this->postJson('api/property/' . $property_id . '/analytic', ['analytic_type_id' => '1', 'value' => 5.5]);

        $response->assertStatus(201);
        $response->assertJson(['created' => true]);

        $json = $response->getContent();
        $array = json_decode($json, true);

        $this->assertEquals(5.5, $array['data']['value']);
        $this->assertEquals('max_Bld_Height_m', $array['data']['analytic_type']['name']);

        // Check that the analytics got updated correctly
        $response = $this->get('api/summary/suburb/Ryde/1');
        $response->assertStatus(200);
        $json = $response->getContent();
        $data = json_decode($json, true);
        $this->assertNotEquals(null, $data);
        $reference = $this->package('max_Bld_Height_m', '5.5', '34.0', '25.5', '100', '0');
        $this->assertSame($reference, $data);
    }

    /*
     * Oh no! I just violated the DRY principle!
     * 
     * TODO: Move this method somewhere and remove the duplicate copy in SummaryTest
     */
    function package($name, $min, $max, $median, $with, $without)
    {
        $reference['summary']['name'] = $name;
        $reference['summary']['minimum_value'] = $min;
        $reference['summary']['maximum_value'] = $max;
        $reference['summary']['median_value'] = $median;
        $reference['summary']['percentage_with_value'] = $with;
        $reference['summary']['percentage_without_value'] = $without;

        return $reference;
    }
}
