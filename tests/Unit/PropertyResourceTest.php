<?php

namespace Tests\Unit;

use App\Http\Resources\Property as PropertyResource;
use App\Models\Property;
use Tests\TestCase;

class PropertyResourceTest extends TestCase
{
    /**
     * A basic unit test example. Using a factory class but only to demonstrate the concept
     * of factories; not really necessary for the test we're running here.
     *
     * @return void
     */
    function testPropertyResource()
    {
        $property = Property::factory()->make();
        $resource = new PropertyResource($property);
        $this->assertSame($property->suburb, $resource->suburb);
        $this->assertSame($property->state, $resource->state);
        $this->assertSame($property->country, $resource->country);
    }
}
